# 质量评估标准-开发指南 

| 大类         | 质量要素                | 内容要求                                                     | 可选/必选 | owner自检 | 资料人员 | 测试人员 |
| ------------ | ----------------------- | ------------------------------------------------------------ | --------- | --------- | -------- | -------- |
| 通用要求     | 正确性                  | 文中所有描述与实际功能实现一致。                             | 必选      | A：满足   | 关注     | 关注     |
|              | 完整性                  | 针对特定方案/特性/功能，覆盖开发者实际开发活动中常用、典型场景，提供相应开发流程和step by step操作指导。无场景缺失，无操作断点。 | 必选      | A：满足   | 关注     | 关注     |
|              | 易理解                  | 逻辑清晰、描述内容具体、写作风格一致、术语统一。             | 必选      | A：满足   | 关注     | 关注     |
|              | 合规性                  | 无抄袭或合规风险。包括但不限于：描述不可与友商高度一致，禁止使用友商特有的概念。 | 必选      | A：满足   | 关注     | 关注     |
| 开发指导要求 | 指导性-场景介绍         | 明确该指导的使用场景，贴近开发者实际开发场景。               | 必选      | A：满足   | 关注     | 关注     |
|              | 指导性-开发流程         | 当操作步骤超过5步（含），应在操作步骤之前提供开发流程图或表，便于开发者阅读理解和记忆。 | 可选      | A：满足   | 关注     | 关注     |
|              | 指导性-接口说明         | 介绍该方案/特性/功能相关的主要接口。接口及其涉及的功能必须在当前版本已支持。 | 可选      | A：满足   | 关注     | 关注     |
|              | 指导性-步骤及示例有效性 | 每个步骤有清晰的执行主体（who），明确操作目的（why）、操作内容（what/how）、场景（when/where）。使用祈使句描述步骤。 | 必选      | A：满足   | 关注     | 关注     |
|              | 指导性-步骤及示例有效性 | 步骤中如果涉及接口调用，需要清晰给出使用的接口及其使用说明。 | 必选      | A：满足   | 关注     | 关注     |
|              | 指导性-步骤及示例有效性 | 保证示例代码的逻辑/语法正确性、书写规范性。涉及用户手机号码、身份证、帐号名等敏感信息均需要打码处理，如186********；涉及IP地址、域名等，需要使用私网IP或相应格式代替，如xx.xx.xx.xx、www.example.com，禁止使用真实IP地址、域名。 | 必选      | A：满足   | 关注     | 关注     |
|              | 指导性-步骤及示例有效性 | 示例代码中关键步骤、以及有开发建议或注意事项的位置需要提供注释说明。 | 必选      | A：满足   | 关注     | 关注     |
|              | 指导性-步骤及示例有效性 | 文档中的片段示例代码直接拷贝进IDE中，放入上下文可以正常编译。    Note：开发提供测试用的工程作为上下文 | 必选      | A：满足   | 关注     | 关注     |
|              | 指导性-步骤及示例有效性 | 文档中的完整示例代码直接拷贝进入IDE中能够运行，并和文档中描述的执行结果一致。 | 必选      | A：满足   | 关注     | 关注     |
|              | 指导性-调测验证         | 开发完成后，如有独立的调测验证操作，需提供指导，以确认操作是否成功。 | 可选      | A：满足   | 关注     | 关注     |
|              | 指导性-常见问题         | 针对开发全流程中可能遇到的典型问题，提供预防、定位、恢复指导，以提高开发效率。 | 可选      | A：满足   | 关注     | 关注     |